// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import router from './router'
import Axios from 'axios'
import less from 'less'

// import Area from './assets/js/area.js'
import './assets/css/common.css'
import './assets/css/introduce.css'
//插件
import Swiper from 'swiper';
import vueSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import VuewechatTitle from 'vue-wechat-title'
import Share from './assets/js/wxShare.js'
import Common from './assets/js/common.js'
Vue.use(VuewechatTitle)
Vue.use(ElementUI);
Vue.use(vueSwiper)
Vue.use(less)
Vue.use(Vuex)
Vue.config.productionTip = false
Vue.prototype.$axios = Axios;
Axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded';
Axios.defaults.baseURL = 'http://api.page.chankang.wang/api/';
// Axios.defaults.baseURL = 'http://t.admin.tc.chankang.wang/api/';
const mApi = "https://api.qinlaila.com/index/index/";
Vue.prototype.$share = Share;
Vue.prototype.$common = Common;
const address= window.localStorage.getItem("address");
const store = new Vuex.Store({
    state: {
      // 电脑城市
        isShowLocation: false,
        locationText: window.localStorage.getItem('locationt')||"全国",

        // 城市数据
        citydata: JSON.parse(window.localStorage.getItem('citydata'))||"",

       // 手机城市
        hid:false,
        address:window.localStorage.getItem("address")||"",
        pcddata: JSON.parse(window.localStorage.getItem('pcddata'))||"",

    },
    mutations: {
      // 电脑城市
       // 点击城市选择按钮
        changeIsShowLocation(state) {
            state.isShowLocation = !state.isShowLocation
        },
        // 选中城市
        changeLocationText(state,text) {
            state.locationText = text;
            window.localStorage.setItem("locationt",state.locationText);
         },

       // 选中城市的省市区
       citydata(state,citydata){
         state.citydata= citydata
         window.localStorage.setItem("citydata",JSON.stringify(state.citydata))
       },


       // 手机城市
        // 点击城市选择按钮
        getlocation1(state){
          state.hid=!state.hid
        },
       // 选中城市
        getlocation2(state,address){
           state.hid=false;
           state.address=address;
           window.localStorage.setItem("address",state.address||"定位失败");
        },
      // 选中城市的省市区
      pcddata(state,pcddata){
        state.pcddata= pcddata
        window.localStorage.setItem("pcddata",JSON.stringify(state.pcddata))
      },
    }
})

// console.log(store);
/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: {
        App
    },
    template: '<App/>',
    store
})
