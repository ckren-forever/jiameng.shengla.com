		getHotCity();
		getAllCity();

		// 获取热门城市
		function getHotCity() {
			let url = "https://api.qinlaila.com/index/index/getHotRegion?appid=ck100&appsecret=ck100secert"
			$.ajax({
				url,
				type: 'get',
				dataType: 'json',
				success: (data) => {
					let hotstr = '';
					let hotcity = data.data;
					hotcity.forEach((item, index) => {
						hotstr += "<a data-v-171c9eb9='' class='city-item' href='#'>" + item.region_name + "</a>";
					})
					$(".city-list").html(hotstr);
          $("#searchInput").value="1";

          // 选择
          $(".city-list").on("click", ".city-item", function() {
               $(".location1").text($(this).text());
          })


				},
				error: error => {
					console.log("获取数据失败")
				}
			})
		}

		//获取所有城市
		function getAllCity() {
			let url = "https://api.qinlaila.com/index/index/getAllCity?appid=ck100&appsecret=ck100secert";
			$.ajax({
				url,
				type: 'get',
				dataType: 'json',
				success: (data) => {
					let allCity = data.data;
					allCity.sort(function(a, b) {
						if (b.first_word > a.first_word) {
							return -1;
						}
						if (b.first_word < a.first_word) {
							return 1;
						}
					});
					// console.log(allCity)

					// 搜索城市
					$("#searchInput").keyup(() => {
						let valuearr = [];
						let value = $("#searchInput").val();
						if(value){
							allCity.forEach((item, index) => {
                let result=-1;
								result = item.region_name.indexOf(value);

								if (result!=-1) {
									valuearr.push(item.region_name);
                  }

									$(".scroll-area").hide();
									$(".aside").hide();

							})

						}else{

								$(".scroll-area").show();
								$(".aside").show();
						}

            // 渲染
            let sstr="";
            valuearr.forEach((item,index)=>{
            	sstr+="<a data-v-05602374='' class='city-row'>"+item+"<hr data-v-05602374='' class='city-row-line'></a>";
            })
            if(value&&sstr==""){
              sstr="<div class='empty' >暂无符合条件的城市</div>"
            }
            $(".search-content").html(sstr);

            // 选择
            $(".search-content").on("click", ".city-row", function() {
                 $(".location1").text($(this).text());
            })

					})


					// 获取相同字母所对应的城市
					let city = [];
					let s = 0;
					for (let i = 0; i < allCity.length; i = i + s) {
						let b = [allCity[i].region_name];
						s = 1;
						for (let j = i + 1; j < allCity.length; j++) {
							if (allCity[j].first_word === allCity[i].first_word) {
								s++;
								let b1 = allCity[j].region_name;
								b.push(b1);
							}
						}
						let city1 = {
							cityword: allCity[i].first_word,
							cityname: b
						};
						city.push(city1);
					}
					// console.log(city)


					// 首字母和城市显示到html
					let allstr = "";
					let wordstr = "";
					let astr = "";
					city.forEach((item, index) => {
						let citystr = "";
						item.cityname.forEach((item1, index1) => {
							citystr += " <a data-v-05602374='' class='city-row'>" + item1 +
								"<hr data-v-05602374='' class='city-row-line'></a>";
						})
						astr += "<li data-v-1022a894='' class='cell'><a data-v-1022a894=''  class='char' href='#" + item.cityword +
							"'><span data-v-1022a894=''>" + item.cityword + "</span></a></li>"
						wordstr = "<h2 data-v-62ae22d0='' class='city-set-char' id='" + item.cityword + "'>" + item.cityword +
							"</h2>" + citystr;
						allstr += "<div class='city1' data-v-410f2c47=''><div data-v-62ae22d0='' data-v-410f2c47='' class='city-set'>" + wordstr +
							"</div></div>";
					})
					let aastr =
						"<li data-v-1022a894='' class='cell'><a data-v-1022a894=''  class='char'><span data-v-1022a894=''  class='icon-city-star-container'><i data-v-ae560058='' data-v-1022a894='' class='iconfont2 icon-star' style='font-size: 8px;'></i></span></a></li>" +
						astr;
					$(".allcity").html(allstr);
					$(".aside").html(aastr);

					// 选择
					$(".allcity").on("click", ".city1>.city-set>.city-row", function() {
					     $(".location1").text($(this).text());
					})

				},
				error: error => {
					console.log("获取数据失败")
				}
			})
		}
