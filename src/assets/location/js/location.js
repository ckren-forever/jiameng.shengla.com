setTimeout(function(){
    let newstr = window.localStorage.getItem('newstr') || "";
    $("body").click(() => {
        if ($(".city-choose>span").text() == "城市") {
            newstr = ""
        } else {
            newstr = $(".city-choose>span").text();
        }
    });
    window.onbeforeunload = function() {
        window.localStorage.setItem('newstr', newstr);
    }
    getHotCity();
    getAllCity();
    getlocation("https://api.qinlaila.com/index/index/get_province?appid=ck100&appsecret=ck100secert&order=first_word&order_desc=asc");
},1000)

// 获取热门城市
function getHotCity() {
    let url = "https://api.qinlaila.com/index/index/getHotRegion?appid=ck100&appsecret=ck100secert"
    $.ajax({
        url,
        type: 'get',
        dataType: 'json',
        success: (data) => {
            let hotstr = '';
            let hotcity = data.data;
            hotcity.forEach((item, index) => {
                hotstr += "<a href='#' class='link city'>" + item.region_name + "</a>";
            })
            $(".citylist .r-info").html(hotstr);
        },
        error: error => {
            console.log("获取数据失败")
        }
    })
}

function getAllCity() {
    let url = "https://api.qinlaila.com/index/index/getAllCity?appid=ck100&appsecret=ck100secert";
    $.ajax({
        url,
        type: 'get',
        dataType: 'json',
        success: (data) => {
            let allCity = data.data;
            allCity.sort(function(a, b) {
                if (b.first_word > a.first_word) {
                    return -1;
                }
                if (b.first_word < a.first_word) {
                    return 1;
                }
            });
            // 搜索城市
            $(".search-text").keyup(() => {
                let valuearr = [];
                let value = $(".search-text").val();
                if (value) {
                    allCity.forEach((item, index) => {
                        let result = item.region_name.indexOf(value);
                        if (result + 1) {
                            valuearr.push(item.region_name);
                        }
                    })
                }
                if (valuearr.length) {
                    let searchstr = "";
                    let sstr = "";
                    let not = "";
                    valuearr.forEach((item, index) => {
                        sstr += "<a href='#' class='link city'>" + item + "</a>";
                    })
                    searchstr = "<div class='suggest-city-wapper'>" + sstr + "</div>"
                    $(".suggest-city-containner").show();
                    $(".suggest-city-containner").html(searchstr);
                }
                if (value && !(valuearr.length)) {
                    let notsearchstr = "";
                    notsearchstr = "<div class='no-match-city'>未找到匹配的城市</div>";
                    $(".suggest-city-containner").show();
                    $(".suggest-city-containner").html(notsearchstr);
                }
                if (!value) {
                    $(".suggest-city-containner").hide();
                }
                // 选择
                $(".suggest-city-containner").on("click", ".suggest-city-wapper>.city", function() {
                    $(".search-text").val($(this).text());
                    $(".city-choose>span").text($(this).text());
                    $(".suggest-city-containner").hide();
                })
            })
            // 获取相同字母所对应的城市
            let city = [];
            let s = 0;
            for (let i = 0; i < allCity.length; i = i + s) {
                let b = [allCity[i].region_name];
                s = 1;
                for (let j = i + 1; j < allCity.length; j++) {
                    if (allCity[j].first_word === allCity[i].first_word) {
                        s++;
                        let b1 = allCity[j].region_name;
                        b.push(b1);
                    }
                }
                let city1 = {
                    cityword: allCity[i].first_word,
                    cityname: b
                };
                city.push(city1);
            }
            // 按首字母选择
            // 首字母和城市显示到html
            let allstr = "";
            let wordstr = "";
            city.forEach((item, index) => {
                let astr = "";
                wordstr += "<a href='#city-" + item.cityword + "' class='letter'>" + item.cityword + "</a>"
                item.cityname.forEach((item1, index1) => {
                    astr += "<a href='#' class='link city'>" + item1 + "</a>";
                })
                allstr += "<div class='city-area' id='city-" + item.cityword + "'><span class='city-label'>" + item.cityword + "</span><span class='cities'>" + astr + "</span></div>";
            })
            $(".alphabet .r-info").html(wordstr);
            $(".alphabet-city-area").html(allstr);
        },
        error: error => {
            console.log("获取数据失败")
        }
    })
}
// 异步获取全部省份
async function getregion(url) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url,
            type: 'GET',
            dataType: 'json',
            success: (data) => (resolve(data)),
            error: error => reject(error)
        })
    })
}
//异步根据省份id获取城市
async function getcity(url) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url,
            type: 'get',
            dataType: 'json',
            success: (data) => (resolve(data)),
            error: error => reject(error)
        })
    })
}
//执行异步获取地址
async function getlocation(url) {
    await getregion(url).then((data) => {
        const data1 = data.data;
        let provincecolstr = '';
        let mtprovincestr = '';
        let k = 12;
        data1.forEach((item, index) => {
            mtprovincestr += "<span class='mt-province'>" + item.region_name + "</span>"
            if (!((index + 1) % k)) {
                provincecolstr += "<div class='province-col'>" + mtprovincestr + "</div>"
                mtprovincestr = "";
            }
            if (index == data1.length - 1) {
                provincecolstr += "<div class='province-col'>" + mtprovincestr + "</div>"
            }
        })
        //渲染
        $(".provinces-wrapper").html(provincecolstr);
        //切换
        $(".province-choose").click(() => {
            $(".mt-provinces").toggleClass("active")
        })
        //切换
        $(".city-choose").click(() => {
            if ($(".cities-wrapper").children().length > 0) {
                $(".mt-cities").toggleClass("active")
            }
        })
        //选择
        $(".provinces-wrapper").on("click", ".province-col>.mt-province", function() {
            $(".province-choose>span").text($(this).text());
            let name = $(".province-choose>span").text()
            data1.forEach((item, index) => {
                if (item.region_name === name) {
                    let id = item.region_id;
                    let cityurl = `https://api.qinlaila.com/index/index/get_city?appid=ck100&appsecret=ck100secert&province=${id}`;
                    getcity(cityurl).then((data) => {
                        let result = data.data;
                        let citycolstr = '';
                        let mtcitystr = '';
                        let m = 12;
                        result.forEach((item, index) => {
                            mtcitystr += "<a href='#' class='link mt-city'>" + item.region_name + "</a>"
                            if (!((index + 1) % m)) {
                                citycolstr += "<div class='city-col'>" + mtcitystr + "</div>"
                                mtcitystr = "";
                            }
                            if (index == result.length - 1) {
                                citycolstr += "<div class='city-col'>" + mtcitystr + "</div>"
                            }
                        })
                        //渲染
                        $(".cities-wrapper").html(citycolstr);
                        
                    });
                }
            })
        });
    });
}
