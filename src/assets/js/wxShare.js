import Qs from 'qs'
import Axios from 'axios'
import Wx from 'weixin-js-sdk'
export default {
  share()
  {
    var re_data = Qs.stringify({"rander":"oB09smB5"});
    Axios.post('api/wx_share',re_data).then((res) => {
      Wx.config({
          debug: false,
          appId: res.data.data.signPackage.appId,
          timestamp: res.data.data.signPackage.timestamp,
          nonceStr: res.data.data.signPackage.nonceStr,
          signature: res.data.data.signPackage.signature,
          jsApiList: [
              "checkJsApi",
              "onMenuShareTimeline",
              "onMenuShareAppMessage",
              "onMenuShareQQ",
              "onMenuShareWeibo",
              "onMenuShareQZone"
          ]
      });
      Wx.ready(function() {
        var shareData = {
          title: ''+res.data.data.shareurl.title+'',
          desc: ''+res.data.data.shareurl.desc+'',
          link: ''+res.data.data.shareurl.link+'',
          imgUrl: ''+res.data.data.shareurl.imgUrl+'?time=202007170032'
       };
        Wx.onMenuShareAppMessage(shareData);
        Wx.onMenuShareTimeline(shareData);
        Wx.onMenuShareQQ(shareData);
        Wx.onMenuShareWeibo(shareData);
        Wx.onMenuShareQZone(shareData);
      });
    }).catch((error) => {
      console.log(error)
    });
  }
}

