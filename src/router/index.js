import Vue from 'vue'
import Router from 'vue-router'
import pcIndex from '@/components/pc/Index.vue' 
import Index from '@/components/Index.vue' 

Vue.use(Router)

export default new Router({
    base: '/web/',
    mode: "history",
    routes: [
      {
        path: '/',
        redirect: '/pc/index'
      },
      {
        path: '/pc/index',
        name: 'pcIndex',
        component: pcIndex
      },
      {
        path: '/index',
        name: 'Index',
        component: Index
      },
    ]
})
